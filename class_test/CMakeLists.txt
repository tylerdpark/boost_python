cmake_minimum_required(VERSION 3.5.0)

project(Test_ext)

find_package(PythonLibs 3.6 REQUIRED)
find_package(Boost 1.54.0 REQUIRED)

add_library (
    Test_ext SHARED
    Test.cpp
)

target_compile_features (
    Test_ext
    PUBLIC
        cxx_std_11
)

target_compile_options (
    Test_ext
    PRIVATE
        -Wall
)

include_directories( 
    ../../../include 
    ${PYTHON_INCLUDE_DIRS} 
    ${Boost_INCLUDE_DIRS} 
)

target_link_libraries(
    Test_ext
    python3.6m
    boost_python3 
)

set_target_properties(
   Test_ext
   PROPERTIES PREFIX "")

