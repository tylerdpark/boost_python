#pragma once
#include <boost/python.hpp>
#include <boost/python/numpy.hpp>
//#include <cuda.h>
#include "CudaApi/CudaMemory.h"
//#include "CudaApi/cudaMacro.h"
//#include "CudaApi/cudaComplexExt.h"

// tests if an ndarray is right-to-left major and compact
// e.g. there are no extra strides
bool isCompact(const boost::python::numpy::ndarray& x)
{
    auto nd = x.get_nd();
    auto unit_size = x.get_dtype().get_itemsize();
    auto next_stride = unit_size;
    bool result = true;
    for (auto i = nd - 1; i >= 0; --i)
    {
        result &= (x.strides(i) == next_stride);
        next_stride *= x.shape(i);
    }
    return result;
}

// creates an empty array of the same size and type as x
boost::python::numpy::ndarray empty_like(const boost::python::numpy::ndarray& x)
{
    return boost::python::numpy::empty(x.get_nd(), x.get_shape(), x.get_dtype());
}

// creates an empty array the same size as x but of type T
template<typename T>
boost::python::numpy::ndarray empty_like_type(const boost::python::numpy::ndarray& x)
{
    return boost::python::numpy::empty(x.get_nd(), x.get_shape(), boost::python::numpy::dtype::get_builtin<T>());
}

// returns the number of elements in the array
int size(const boost::python::numpy::ndarray& x)
{
    int result = 1;
    for (int i = 0; i < x.get_nd(); ++i)
        result *= x.shape(i);
    return result;
}

// checks if an array is of type T
template<typename T>
bool checkType(const boost::python::numpy::ndarray& x)
{
    return boost::python::numpy::dtype::get_builtin<T>() == x.get_dtype();
}

// copies a compact array of type T to the gpu and returns smart pointer
template<typename T, typename DeviceT=T>
CudaPtr<DeviceT> copyToDevice(const boost::python::numpy::ndarray& x)
{
    assert(isCompact(x));
    assert(checkType<T>(x));
    return copyToDevice(reinterpret_cast<DeviceT*>(x.get_data()), size(x));
}

// copies a gpu smart pointer into a compact ndarray on the host
// of the same size and type
template<typename T, typename HostT=T>
void copyToHost(const CudaPtr<T>& src, boost::python::numpy::ndarray& dst)
{
    assert(isCompact(dst));
    assert(checkType<HostT>(dst));
    assert(size(dst) == src.elems());
    copyToHost(src, reinterpret_cast<T*>(dst.get_data()));
}

