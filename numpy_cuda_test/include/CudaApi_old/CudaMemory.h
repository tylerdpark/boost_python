#pragma once
#include <memory>
#include <vector>
#include "CudaApi/CudaError.h"
#include "CudaApi/CudaPtr.h"
#include "CudaApi/CudaStream.h"

template<typename T>
CudaPtr<T> deviceAlloc(size_t elems)
{
    T* ptr;
    cudaThrowOnError(cudaMalloc(&ptr, sizeof(T)*elems));
    return CudaPtr<T>(ptr, elems);
}

template<typename T>
CudaPtr<T> hostAlloc(size_t elems)
{
    T* ptr;
    cudaThrowOnError(cudaMallocHost(&ptr, sizeof(T)*elems));
    return CudaPtr<T>(ptr, elems);
}

template<typename T>
CudaPtr<T> copyToDevice(const T* data, size_t elems, const CudaStream& stream=CudaStream::defaultStream())
{
    auto ptr = deviceAlloc<T>(elems);
    cudaThrowOnError(cudaMemcpyAsync(ptr.get(), data, sizeof(T)*elems, cudaMemcpyHostToDevice, stream));
    return ptr;
}

template<typename T>
CudaPtr<T> copyOnDevice(const T* data, size_t elems, const CudaStream& stream=CudaStream::defaultStream())
{
    auto ptr = deviceAlloc<T>(elems);
    cudaThrowOnError(cudaMemcpyAsync(ptr.get(), data, sizeof(T)*elems, cudaMemcpyDeviceToDevice, stream));
    return ptr;
}

template<typename T>
CudaPtr<T> copyOnDevice(const CudaPtr<T>& data, const CudaStream& stream=CudaStream::defaultStream())
{
    auto ptr = deviceAlloc<T>(data.elems());
    cudaThrowOnError(cudaMemcpyAsync(ptr.get(), data.get(), data.bytes(), cudaMemcpyDeviceToDevice, stream));
    return ptr;
}

template<typename T>
CudaPtr<T> copyToDevice(const std::vector<T>& vec, const CudaStream& stream=CudaStream::defaultStream())
{
    return copyToDevice(vec.data(), vec.size(), stream);
}

template<typename T>
void copyToHost(const CudaPtr<T>& src, T* dst, const CudaStream& stream=CudaStream::defaultStream())
{
    cudaThrowOnError(cudaMemcpyAsync(dst, src.get(), sizeof(T)*src.elems(), cudaMemcpyDeviceToHost, stream));
}
