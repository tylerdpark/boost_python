#pragma once
#include <memory>

struct CudaDeleter
{
    void operator()(void* p);
};

template <typename T>
class CudaPtr : public std::unique_ptr<T, CudaDeleter>
{
public:
    using std::unique_ptr<T, CudaDeleter>::get;

    CudaPtr() : nElems_{} {}
    CudaPtr(T* ptr, size_t nElems)
      : std::unique_ptr<T, CudaDeleter>{ptr}
      , nElems_{nElems}
    {}

    // conversion operators for easy use
    operator T const *() const { return get(); }
    operator T       *()       { return get(); }

    size_t elems()       const { return nElems_; }
    size_t bytes()       const { return elems()*sizeof(T); }

private:
    size_t nElems_;
};


template <typename T>
class CudaSharedPtr : public std::shared_ptr<T>
{
public:
    using std::shared_ptr<T>::get;

    CudaSharedPtr() : nElems_{} {}

    CudaSharedPtr(CudaPtr<T>&& ptr)
      : std::shared_ptr<T>{static_cast<std::unique_ptr<T, CudaDeleter>&&>(ptr)}
      , nElems_{ptr.elems()}
    {}

    CudaSharedPtr(T* ptr, size_t nElems)
      : std::shared_ptr<T>{ptr, CudaDeleter()}
      , nElems_{nElems}
    {}

    // conversion operators for easy use
    operator T const *() const { return get(); }
    operator T       *()       { return get(); }

    size_t elems()       const { return nElems_; }
    size_t bytes()       const { return elems()*sizeof(T); }

private:
    size_t nElems_;
};
