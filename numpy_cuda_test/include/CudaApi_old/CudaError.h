#pragma once
#include <iostream>
#include <cuda_runtime_api.h>

inline void cudaThrowOnError(cudaError_t err)
{
    switch (err)
    {
        case cudaSuccess:
            break;
        case cudaErrorMemoryAllocation:
            throw std::bad_alloc();
        default:
            throw std::runtime_error(cudaGetErrorString(err));
    }
}

inline void cudaLogError(cudaError_t err)
{
    if (err != cudaSuccess)
    {
        std::cerr << "CUDA error: " << cudaGetErrorString(err);
    }
}
