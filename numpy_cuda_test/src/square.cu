#include <boost/python.hpp>
#include <boost/python/numpy.hpp>
#include <cuda.h>
#include "CudaApi/CudaMemory.h"
#include "CudaApi/cudaMacro.h"
#include "CudaApi/cudaComplexExt.h"
#include "numpy_cuda.h"

template<typename T>
__global__ void cudaSquare(T const* in, T* out, int size)
{
    int const k = threadIdx.x + blockDim.x*blockIdx.x;
    if (k < size)
        out[k] = in[k]*in[k];
}

namespace p = boost::python;
namespace np = p::numpy;

template<typename T, typename DeviceT=T>
np::ndarray squareT(const np::ndarray& x)
{
    auto n = size(x);

    // copy/allocate data on the device
    auto dx = copyToDevice<T, DeviceT>(x);
    auto dy = deviceAlloc<DeviceT>(n);

    // perform the cuda call
    constexpr int blockDim = 256;
    const int gridDim = (n - 1)/blockDim + 1;
    cudaSquare<DeviceT><<<gridDim, blockDim>>>(dx, dy, n);
    cudaCheckError();

    // copy data off the device into an ndarray
    auto y = empty_like(x);
    copyToHost<DeviceT, T>(dy, y);
    return y;
}

// performs the element-wise square of a float array
np::ndarray square(np::ndarray x)
{
    if (checkType<float>(x))
        return squareT<float>(x);
    else if (checkType<std::complex<float>>(x))
        return squareT<std::complex<float>, cuComplex>(x);
    else if (checkType<double>(x))
        return squareT<double>(x);
    else if (checkType<int>(x))
        return squareT<int>(x);
    else
        throw std::runtime_error(
                std::string("unsupported dtype") +
                std::string(p::extract<char const *>(p::str(x.get_dtype()))));
}

BOOST_PYTHON_MODULE(square_ext)
{
    using namespace boost::python;
    Py_Initialize();
    np::initialize();
    def("square", square);
}

